/*jshint esversion: 7 */

/*
*/

plugin.run = options => {
    let origXml, imgWithGridBlob;

    options.elem.querySelector('#gridSize').value = Math.ceil(Math.min(options.metadata.width, options.metadata.height) / 10);
    const unit = options.metadata.unit || 'inches';
    options.elem.querySelector('.designUnit').innerText = unit;//unitSymbols[unit];

    const preview = _.debounce(function() {
        URL.revokeObjectURL(options.elem.querySelector('img').getAttribute('src'));
        const gridSize = parseFloat(options.elem.querySelector('#gridSize').value);
        if (gridSize > Math.min(options.metadata.width, options.metadata.height) / 100) {
            const xml = origXml.cloneNode(true);
            const viewBox = xml.getAttribute('viewBox').split(' ').map(parseFloat);
            const grid = xml.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
            const lineProps = {
                stroke: '#888',
                'stroke-width': Math.min(viewBox[2], viewBox[3]) * 0.001,
                // opacity: 0.5,
            };
            const offset = lineProps['stroke-width'] / 2;
            for (let x = viewBox[0] + offset; x <= viewBox[0] + viewBox[2]; x += viewBox[2] * gridSize / options.metadata.width) {
                const line = grid.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
                line.setAttribute('x1', x);
                line.setAttribute('y1', viewBox[1]);
                line.setAttribute('x2', x);
                line.setAttribute('y2', viewBox[1] + viewBox[3]);
                for (const [k, v] of Object.entries(lineProps)) {
                    line.setAttribute(k, v);
                }
            }
            for (let y = viewBox[1] + offset; y <= viewBox[1] + viewBox[3]; y += viewBox[3] * gridSize / options.metadata.height) {
                const line = grid.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
                line.setAttribute('x1', viewBox[0]);
                line.setAttribute('y1', y);
                line.setAttribute('x2', viewBox[0] + viewBox[2]);
                line.setAttribute('y2', y);
                for (const [k, v] of Object.entries(lineProps)) {
                    line.setAttribute(k, v);
                }
            }
            const svg = new XMLSerializer().serializeToString(xml);
            imgWithGridBlob = new Blob([svg], {type: 'image/svg+xml'});
            options.elem.querySelector('img').setAttribute('src', URL.createObjectURL(imgWithGridBlob));
        } else {
            options.elem.querySelector('img').setAttribute('src', URL.createObjectURL(options.input));
        }
    }, 200);
    options.elem.querySelector('#gridSize').addEventListener('input', preview);

    if (options.input.type == 'image/svg+xml') {
        const reader = new FileReader();
        reader.addEventListener('loadend', () => {
            origXml = new DOMParser().parseFromString(reader.result, "image/svg+xml").children[0];
            preview();
        });
        reader.readAsText(options.input);
    } else {
        const inputURL = URL.createObjectURL(options.input);
        imgutil.getImageFromURL(inputURL).then(img => {
            URL.revokeObjectURL(inputURL);
            origXml = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            origXml.setAttribute('version', "1.1");
            origXml.setAttribute('preserveAspectRatio', "xMinYMin meet");
            origXml.setAttribute('viewBox', [0, 0, img.width, img.height].join(' '));
            origXml.setAttribute('width', img.width); // so when shown by other viewers, is presented at a decent size
            origXml.setAttribute('height', img.height);
            imgutil.blobToDataURL(options.input).then(url => {
                const imageElem = document.createElementNS('http://www.w3.org/2000/svg', 'image');
                // imageElem.setAttribute('preserveAspectRatio', 'none');
                imageElem.setAttribute('href', url);
                imageElem.setAttribute('x', 0);
                imageElem.setAttribute('y', 0);
                imageElem.setAttribute('width', img.width);
                imageElem.setAttribute('height', img.height);
                origXml.appendChild(imageElem);
                preview();
            });
        });
    }

    options.elem.querySelectorAll('.pluginCancel').forEach(elem => elem.addEventListener('click', () => options.output()));
    options.elem.querySelectorAll('.pluginApply').forEach(elem => elem.addEventListener('click', () => {
        options.output(imgWithGridBlob);
    }));

    options.elem.querySelector('.zoomView').addEventListener('input', function() {
        updateLayout();
        if (this.checked) { // scroll to zoomed center
            options.elem.querySelector('.pluginViewWrapper').scroll({
                left: (parseFloat(options.elem.querySelector('.pluginViewWrapper img').scrollWidth) - options.elem.querySelector('.pluginViewWrapper').clientWidth) / 2,
                top: (parseFloat(options.elem.querySelector('.pluginViewWrapper img').scrollHeight) - options.elem.querySelector('.pluginViewWrapper').clientHeight) / 2,
                behavior: 'instant'
            });
        }
    });

    options.elem.querySelector('.changeGridSize').addEventListener('click', () => {
        dialogs.prompt('Grid', `Size in ${unit}`, size => {
            if (size) {
                options.elem.querySelector('#gridSize').value = parseFloat(size);
                preview();
            }
        }, ['Cancel', 'Set'], options.elem.querySelector('#gridSize').value);
    });

    function updateLayout() {
        const bigUI = options.elem.clientWidth > 768;
        const zoomed = options.elem.querySelector('.zoomView').checked;
        Object.assign(options.elem.querySelector('.pluginView img').style, zoomed ? {
            width: '200%',
            height: '',
            'object-fit': 'unset',
        } : {
            width: '100%',
            height: '100%',
            'object-fit': 'contain',
        });
        if (bigUI) {
            options.elem.querySelector('.smallPluginControls').style.display = 'none';
            const ctrlWidth = 20; // in em
            Object.assign(options.elem.querySelector('.bigPluginControls').style, {
                position: 'absolute',
                top: options.suggestedPadding.top,
                bottom: options.suggestedPadding.bottom,
                left: options.suggestedPadding.left,
                right: 'unset',
                width: ctrlWidth + 'em',
                'overflow-y': 'auto',
                display: '',
            });
            const viewLeft = `calc(${ctrlWidth + 1}em + ${options.suggestedPadding.left}`;
            Object.assign(options.elem.querySelector('.pluginView').style, {
                position: 'relative',
                top: 0,
                height: '100%',
                left: viewLeft,
                width: `calc(${options.elem.clientWidth}px - ${viewLeft})`,
            });
        } else {
            options.elem.querySelector('.bigPluginControls').style.display = 'none';
            const btnHeight = options.elem.querySelector('.pluginToolbarButton').clientHeight;
            Object.assign(options.elem.querySelector('.smallPluginControls').style, {
                position: 'absolute',
                bottom: 0,
                top: 'unset',
                right: 'unset',
                left: 'unset',
                width: '100%',
                height: btnHeight + 'px',
                display: ''
            });
            Object.assign(options.elem.querySelector('.pluginView').style, {
                position: 'relative',
                top: 0,
                right: 0,
                left: 0,
                width: 'unset',
                height: (options.elem.clientHeight - btnHeight) + 'px',
            });
        }
    }
    updateLayout();
    new ResizeObserver(() => { updateLayout(); }).observe(options.elem);
};
