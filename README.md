# README #

Rapid Resizer plugin sample.

### What is this repository for? ###

Locally develop Rapid Resizer plugins.

### How do I get set up? ###

Clone this repository:

    git clone https://bitbucket.org/prcode/plugin-sample plugin-YOUR-PLUGIN-NAME
    cd plugin-YOUR-PLUGIN-NAME
    rm -rf .git
    git init

To use your plugin live: `npm run start`

Edit the manifest.json, package.json, and plugin.{css,html,js} files.

#### properties of `options` (parameter to plugin.run):

elem
: HTML element that will contain a copy of plugin.html.

input
: Input blob (if an input was type specified in the plugin's manifest). Typically, a PNG or SVG.

metadata
: Extra information about the object that can be modified by the plugin:

    * width: undefined if the design hasn't been given a size yet
    * height: undefined if the design hasn't been given a size yet
    * unit: 'inches', 'feet', 'cm', or 'mm'

output
: Function to be called with the plugin's output blob (if any) when the plugin's done. Call output() with no parameters to have the plugin cancel.

Plugins should be fully responsive, working on all devices: computer, tablet, phone, mouse, touch, keyboard, stylus.

When outputting an SVG blob, set the width, height, and viewBox in the svg tag:
```
options.output(new Blob([`<svg preserveAspectRatio="xMinYMin meet" x="0" y="0" width="${w}" height="${h}" viewBox="0 0 ${w} ${h}" version="1.1" style="background-color: white;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">...`], {type: 'image/svg+xml'}));
```

### API

TODO: document imgutil.js utility functions