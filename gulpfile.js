/*jshint esversion: 9 */

const gulp = require('gulp');

function clean() {
    return require('child_process').exec('rm -rf dist');
}

function lintHtml() {
    const htmllint = require('gulp-htmllint');
    return gulp.src('plugin.html')
        // - echo '{"tag-pair":false, "id-unique":false, "attr-no-duplication":true, "title-require":false, "attr-value-double-quotes":false, "doctype-first":false, "attr-lowercase":false}' > .htmlhintrc # https://github.com/yaniswang/HTMLHint/wiki/Rules
       .pipe(htmllint({
            rules: {
                'line-end-style': false,
                'line-no-trailing-whitespace': false,
                'attr-quote-style': false,
                'id-class-style': false,
                'attr-bans': false,
                'indent-width': false,
                'tag-bans': false,
                'img-req-alt': false,
                'attr-name-style': false,
                'img-req-src': false,
                'tag-close': false,
                'link-req-noopener': false,
                'id-no-dup': false
            },
            failOnError: true
        }));
}

function lintJs() {
    // - for f in *.js; do node --check $f || exit $?; done # syntax checks javascript - could lint too
    const eslint = require('gulp-eslint-new');
    return gulp.src('plugin.js')
        .pipe(eslint({
            rules: {},
            // parserOptions: {
            //     ecmaVersion: 9
            // },
            envs: ["es2018"]
        }))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
}

function copy() {
    return gulp.src(['manifest.json', 'lib/*']).pipe(gulp.dest('dist'));
}

function css() {
    const cleanCSS = require('gulp-clean-css');
    return gulp.src('plugin.css', {allowEmpty: true})
        .pipe(cleanCSS())
        .pipe(gulp.dest('dist'));
    // const stylus = require('gulp-stylus');
    // return gulp.src('style.styl').pipe(stylus({
    //     compress: true
    // })).pipe(gulp.dest('dist'));
}

function html() {
    const fs = require('fs');
    const htmlmin = require('gulp-htmlmin');
    return gulp.src('plugin.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            // minifyCSS: true,
            // minifyJS: true,
            // processScripts: "text/x-handlebars-template"
        }))
        .pipe(gulp.dest('dist'));
}

function js() {
    const sourcemaps = require('gulp-sourcemaps');
    const babel = require('gulp-babel');
    const uglify = require('gulp-uglify-es').default;
    return gulp.src('plugin.js')
        .pipe(sourcemaps.init())
        .pipe(babel({ // node_modules/.bin/babel $f --source-maps inline
            presets: ['@babel/env']
        }))
        .pipe(uglify({
            mangle: { // https://github.com/mishoo/UglifyJS
                // toplevel: false
            },
            compress: {
                // drop_console: true
            },
            output: {
                // beautify: false,
                preamble: "/* Copyright Patrick Roberts https://www.rapidresizer.com */" // make sure to only add this to my code, not other's libraries
            }
        }))
        .pipe(sourcemaps.write('maps')) // - don't allow sourcemaps on the server or they can undo the uglifying! publicPath: 'https://localhost:5050/',
        .pipe(gulp.dest('dist'));
}

function zip() {
    const zip = require('gulp-zip');
    return gulp.src(['dist/*', '!dist/maps'])
        .pipe(zip('plugin.zip'))
        .pipe(gulp.dest('dist'));
    // const zip = new JSZip();
    // zip.file("Hello.txt", "Hello World\n");
    // zip.generateNodeStream({type:'nodebuffer',streamFiles:true}).pipe(fs.createWriteStream('out.zip')).on('finish', function () {
    //     console.log("out.zip written.");
    // });  
}

// function img() {
//     const imagemin = require('gulp-imagemin');
//     return gulp.src('web/img/*')
// 		// .pipe(imagemin()) // it botches spinner.svg
// 		.pipe(gulp.dest('dist/img'));
// }

exports.default = gulp.series(clean, gulp.parallel(lintHtml, lintJs), gulp.parallel(copy, css, html, js), zip);
exports.clean = clean;
exports.lint = gulp.parallel(lintHtml, lintJs);
exports.watch = function() {
    const harness = require('plugin-harness');
    const server = harness.start();
    function reload(callback) {
        server.reload();
        callback();
    }
    gulp.watch('plugin.*', gulp.series(exports.default, reload));
    // ? open web browser
};
